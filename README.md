### Setup ###

Dog Cat classification is the basic example of Image Classification where the machine will be able to identify whether the given image is of dog or cat. Moreover, it will also tell the probability of the image falling under the two categories.

### Setup ###

* git clone the project
* Download the spyder for python. (It will be easier to run the code with this)
* Point to the directory where the file and dataset folder is stored.
* Open the file
* Select all code of file and press ctrl+enter

### Detail Documentation ###
[Project Documentation](https://docs.google.com/document/d/197kd6YXdm_n9iy-Re5oa5fdpL5Mq0Rl_swAyBZ72-S0/edit?usp=sharing)